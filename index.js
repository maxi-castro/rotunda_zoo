function Animal(species, sentence) {
    this.species = species;
    this.sentence = sentence;

    this.speak = function(say) {
        const words = say.split(" ");
        const speech = [];

        words.forEach(element => {
            speech.push(`${element} ${this.sentence}`);
        });

        const parsedSpeech = speech.join(' ');
        console.log(parsedSpeech);
    }
  };

  const lion = new Animal('Lion', 'roar');
  lion.speak("I'm a lion");

  const tiger = new Animal('Tiger', 'grrr');
  tiger.speak("Lions suck");
